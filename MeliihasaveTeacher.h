#ifndef _METHODSAVE_H
#define _METHODSAVE_H
#include <exception>
#include <string>
#include "teacher.h"
#include "teacherDB.h"
#include "ArrayGeneric.h"
#include "departmentDB.h"

// departmentAPI niz departmenta, mora biti globalan
// teacherAPI niz profesora, ~//~

teacherDB& teacherDB::push( Teacher& newTeacher ){
  Teacher* temp=STD.getPointer()[newTeacher.getId()]; //teacher na indeksu ID-a novog  
  int i=1;
  for(; i<=n; ++i)
    if(teacherAPI.getPointer()[i].getJmbg()==newTeacher.getJmbg()){ // ako je isti jmbg novog i i-tog teachera radi se o istom
      if(teacherAPI.getPointer()[i].getID()==newTeacher.getId()){ // provjeravamo da li im je isti i ID
        teacherAPI.getPointer()[i]=newTeacher; // samo ga prepisemo
        break;
      }
      else{ // ako se radi update ID-a 
        teacherAPI.getPointer()[i].deleteTeacher();
        if(temp->getId()==-1){
          *temp=newTeacher;
          break;
        }
        else throw std::string("Taj ID je zauzet.");
      }
    }
  // ako nema profesora sa tim jmbg-om dodaje se novi 
  if(i==n) *temp=newTeacher;
increment();
}
}

teacherDB& teacherDB::save(int ID, std::string firstName, std::string lastName, std::string birthDate, std::string email, std::string gender, std::string jmbg,std::string title, department& object ){ 
  int temp=departmentAPI.getID(object.getName()); // pronalazi proslijedjeni department u nizu i njegov stvarni ID
  if(temp==-1){ // ukoliko ne postoji takav department u nizu 
    departmentAPI.push(object); // dodajemo novi department u niz
    temp=departmentAPI.size(); // sada je temp novi ID
  }
  if(temp==object.getID()) //  ako su jednaki proslijedjeni i stvarni ID-ovi
    teacherAPI.push(Teacher(ID, firstName, lastName, birthDate, email, gender, jmbg,title, temp));
  else throw std::string("Unijeli ste pogresan ID departmenta. ");   
}

#endif
