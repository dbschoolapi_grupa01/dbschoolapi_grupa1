#ifndef _METHODSAVE_H
#define _METHODSAVE_H
#include <exception>
#include <string>
#include "student.h"
#include "studentDB.h"
#include "ArrayGeneric.h"
#include "departmentDB.h"

// DepartmentAPI niz departmenta, mora biti globalan
// StudentAPI niz studenata, ~//~

studentDB& studentDB::push( Student& newStudent ){
  Student* temp=StudentAPI.getPointer()[newStudent.getId()]; //student na indeksu ID-a novog studenta 
  int i=1;
  for(; i<=n; ++i)
    if(StudentAPI.getPointer()[i].getJmbg()==newStudent.getJmbg()){ // ako je isti jmbg novog i i-tog studenta radi se o istom studentu
      if(StudentAPI.getPointer()[i].getID()==newStudent.getId()){ // provjeravamo da li im je isti i ID
        StudentAPI.getPointer()[i]=newStudent; // samo ga prepisemo
        break;
      }
      else{ // ako se radi update ID-a 
        StudentAPI.getPointer()[i].deleteStudent();
        if(temp->getId()==-1){
          *temp=newStudent;
          break;
        }
        else throw std::string("Taj ID je zauzet.");
      }
    }
  // ako nema studenta sa tim jmbg-om dodaje se novi student
  if(i==n) *temp=newStudent;
increment();
}
}

studentDB& studentDB::save(int ID, std::string firstName, std::string lastName, std::string birthDate, std::string email, std::string gender, std::string jmbg, department& object ){ 
  int temp=DDB.getID(object.getName()); // pronalazi proslijedjeni department u nizu i njegov stvarni ID
  if(temp==-1){ // ukoliko ne postoji takav department u nizu 
    DDB.push(object); // dodajemo novi department u niz
    temp=DDB.size(); // sada je temp novi ID
  }
  if(temp==object.getID()) //  ako su jednaki proslijedjeni i stvarni ID-ovi
    STDB.push(Student(ID, firstName, lastName, birthDate, email, gender, jmbg, temp));
  else throw std::string("Unijeli ste pogresan ID departmenta. ");   
}

#endif
