#include "departmentDB.h"

departmentDB& departmentDB::save(int ID,std::string name)
{
  if(ID>0&&ID<ArraySize()){
    getPointer()[ID-1].setName(name);
    return *this;
  }
}

int departmentDB::getID(std::string name){
  for(int i=1; i<=ArraySize(); ++i)
    if(getPointer()[i].getName()==name )
      return i;
  return -1;
}

departmentDB& departmentDB::push(departments& newDepartment){
 newDepartment.setID(size()); // postavljamo ID na broj indeksa
 getPointer()[size()]=newDepartment;
 increment();
}

void departmentDB::saveToFile(const std::string & fileName)
{
  std::ofstream outputfile;
  outputfile.open(fileName);
  if(outputfile.is_open()){
        outputfile<<"Id | name"<< std::endl;
        for(int i=1;i<=ArraySize();++i){
                if(getPointer()[i].getID()!=-1){
            getPointer()[i].saveToFile(outputfile);
        }}
          outputfile.close();

  }
  else
  {
    throw std::runtime_error("File didn't open correctly. Closing program !!!\n");
  }
}
