#include "departments.h"

/* Member functions */
departments::departments(const std::string& Name):Name_(Name){}
// End Constructor


/* Operators */
std::ostream & operator<<(std::ostream & output , const departments & departments)
{
    output<<"Department: " <<departments.getName()<< std::endl;
    return output;
}// End std::ostream & operator<<(std::ostream & , const departments)

departments & departments::operator=(const departments &b)
{
    Name_=b.Name_;
    return *this;
}


//Function that saves departments to the file
void departments::saveToFile(std::ofstream & output) const
{
    output <<ID_<<", "<< Name_ <<std::endl;

}

