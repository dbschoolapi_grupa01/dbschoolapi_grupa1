#ifndef PERSON_H
#define PERSON_H
#include<string>
#include<iostream>
#include "validate.h"
class Person{
  protected:
    std::string _firstName;
    std::string _lastName;
    std::string _birthDate;
    std::string _eMail;
    std::string _gender;
    std::string _jmbg;
  public:

    //Member functions
    Person() = default;
    ~Person() = default;
    Person(std::string firstName,std::string lastName,std::string birthDate,std::string eMail,std::string gender,std::string jmbg);

    //Getters
    std::string& getFirstName(){ return _firstName; }
    std::string& getLastName(){ return _lastName; }
    std::string& getBirthDate(){ return _birthDate; }
    std::string& getEmail(){ return _eMail; }
    std::string& getGender(){ return _gender; }
    std::string& getJmbg(){ return _jmbg; }
   
    //Setters 
    void setFirstName(const std::string &firstName){ _firstName = firstName; }
    void setLastName(const std::string &lastName){ _lastName = lastName; }
    void setBirthDate(const std::string &birthDate){ validateBirthDate(birthDate); _birthDate = birthDate; }
    void setEmail(const std::string &eMail){ validateEmail(eMail); _eMail = eMail; }
    void setGender(const std::string &gender){ validateGender(gender); _gender = gender; }
    void setJmbg(const std::string &jmbg){ validateJMBG(jmbg); _jmbg = jmbg; }
    
   // friend std::ostream& operator<<(std::ostream &out,const Person &other);
    
   // virtual std::ostream& print(std::ostream&) const = 0;
};

//std::ostream& operator<<(std::ostream &out,const Person &other){
  //return other.print(out);
//}

Person::Person(std::string firstName,std::string lastName,std::string birthDate,std::string eMail,std::string gender,std::string jmbg){
    //Input first and last name
    _firstName = firstName;
    _lastName = lastName;
    //Validate and input birth date
    validateBirthDate(birthDate);
    _birthDate = birthDate;
    //Validate and input eMail
    validateEmail(eMail);
    _eMail = eMail; 
    //Validate and input gender
    validateGender(gender);
    _gender = gender; 
    //Validate and input JMBG
    validateJMBG(jmbg);
    _jmbg = jmbg;

}
#endif
