#ifndef STUDENTDB_H
#define STUDENTDB_H

#include"student.h"
#include"ArrayGeneric.h"

class studentDB: public ArrayGeneric<Student>
{
  public:
        Student& getByID(const int& n){
            if((getPointer()[n]).getId()!=-1)
                return getPointer()[n];
            else
                throw std::runtime_error("ID doesn't exist. Closing program !!!\n");
            }
        studentDB& save(const int& i,const std::string&,const std::string&,const std::string&,const std::string&,const std::string&,const std::string&,const int&);

      void saveToFile(const std::string & )  ;
    protected:

    private:
};

#endif // STUDENTDB_H
