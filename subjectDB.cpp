void subjectDB::saveToFile(const std::string & fileName)
{
  std::ofstream outputfile;
  outputfile.open(fileName);
  if(outputfile.is_open()){
        outputfile<<"Id | name | ects | abbrevation"<< std::endl;
        for(int i=1;i<=ArraySize();++i){
            getPointer()[i].saveToFile(outputfile);
        }
          outputfile.close();

  }
  else
  {
    throw std::runtime_error("File didn't open correctly. Closing program !!!\n");
  }
}
