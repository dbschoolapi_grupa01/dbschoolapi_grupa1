#ifndef SUBJECTTEACHER_H
#define SUBJECTTEACHER_H

#include"listaNizom.h"
#include<iostream>
using namespace std;

class Node
{
    public:
        Node():Link(nullptr),TeacherIDs(5){};
        ~Node(){};
        int& getSubjectID(){return SubjectID;}
        Node* getLink(){return Link;}
        void setLink(Node* n){Link=n;}
        void setSubjectID(const int&i){SubjectID=i;}
        void addTeacher(const int& i){TeacherIDs.dodajNaKraj(i);};
        void getTeacherIDs(){TeacherIDs.print();};
        int NumberOfTeachers(){return TeacherIDs.velicina();};
        ListaNizom<int>& getTeachers(){return TeacherIDs;};
        void saveToFile(std::ofstream &);


    private:
        int SubjectID;
        ListaNizom<int> TeacherIDs;
        Node* Link;
};

class subjectTeacherDB{
    private:
        Node *first;
        Node *last;
        int Size;
    public:
        int size(){return Size;}
        subjectTeacherDB():first(nullptr),last(nullptr),Size(0){}
        void destroyList();
        ~subjectTeacherDB();
        void insert(const int& s,const int& p);
        Node* find(const int& s);
        void ispis();
        void saveToFile(const std::string & );


};

#endif // SUBJECTTEACHER_H
