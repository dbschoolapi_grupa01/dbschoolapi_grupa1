#include <iostream>
#include<fstream>
#include"teacherDB.h"
#include"subjectDB.h"
#include"studentDB.h"
#include<string>
using namespace std;

    studentDB studentAPI;
    teacherDB teacherAPI;
    subjectDB subjectAPI;

    string FileStudent("DB/student");
    string FileTeacher("DB/teacher");
    string FileSubject("DB/subject");

    fstream StudentTxt(FileStudent.c_str());
    fstream TeacherTxt(FileTeacher.c_str());
    fstream SubjectTxt(FileSubject.c_str());


void InsertTeacher(const vector<string>& Data){
    if(Data.size()==12){ //ako je manje ili vece od 12 neki podaci fale ili ih je viska
        int i=stoi(Data[2]);
        if(i>=1&&i<=teacherAPI.size()&&(teacherAPI[i].getId()!=-1))
            teacherAPI[i]=Teacher(i,Data[3],Data[4],Data[5],Data[6],Data[7],Data[8],Data[9],stoi(Data[10]));
        if(i>teacherAPI.size())
            teacherAPI.PushBack(Teacher(i,Data[3],Data[4],Data[5],Data[6],Data[7],Data[8],Data[9],stoi(Data[10])));
        teacherAPI.saveToFile(FileTeacher);
    }
    else
        throw invalid_argument("Form isn't valid.");
    }
void SelectTeacher(const string& temp){
        if(temp=="ALL"){
            for(int i=1;i<=teacherAPI.size();i++)
                cout<<teacherAPI.getByID(i)<<endl;
        }
        else
            cout<<teacherAPI.getByID(stoi(temp))<<endl;
    }
void DeleteTeacher(const string& temp){
    int index=stoi(temp);
    teacherAPI.getByID(index).setId(-1); //podaci vise nece biti validni,sto je jedanko brisanju
    teacherAPI.saveToFile(FileTeacher);
    }
void SelectStudent(const string& temp){
        if(temp=="ALL"){
            for(int i=1;i<=studentAPI.size();i++)
                cout<<studentAPI.getByID(i)<<endl;
        }
        else
            cout<<studentAPI.getByID(stoi(temp))<<endl;
}
void DeleteStudent(const string& temp){
    int index=stoi(temp);
    studentAPI.getByID(index).setId(-1); //podaci vise nece biti validni,sto je jedanko brisanju
    studentAPI.saveToFile(FileStudent);
    }
void InsertStudent(const vector<string>& Data){
    int i=stoi(Data[2]);
    if(Data.size()==11){ //ako je manje ili vece od 12 neki podaci fale ili ih je viska
            //provjera da li je dati id validan,
        if(i>=1&&i<=studentAPI.size()&&(studentAPI[i].getId()!=-1)) //ako student sa datim id nije vise validan,npr. izbrisan
            studentAPI[i]=Student(i,Data[3],Data[4],Data[5],Data[6],Data[7],Data[8],stoi(Data[9]));
        if(i>studentAPI.size()) //ako studenta treba dodati u bazu
            studentAPI.PushBack(Student(i,Data[3],Data[4],Data[5],Data[6],Data[7],Data[8],stoi(Data[9])));
        studentAPI.saveToFile(FileStudent);
    }
    else
        throw invalid_argument("Form isn't valid.");
    }
void SelectSubject(const string& temp){
        if(temp=="ALL"){
            for(int i=1;i<=subjectAPI.size();i++)
                cout<<subjectAPI.getByID(i)<<endl;
        }
        else
            cout<<subjectAPI.getByID(stoi(temp))<<endl;
}
void DeleteSubject(const string& temp){
    int index=stoi(temp);
    subjectAPI.getByID(index).setId(-1); //podaci vise nece biti validni,sto je jedanko brisanju
    subjectAPI.saveToFile(FileSubject);
    }
void InsertSubject(const vector<string>& Data){
    int i=stoi(Data[2]);
    if(Data.size()==7){ //ako je manje ili vece od 12 neki podaci fale ili ih je viska
        if(i>=1&&i<=subjectAPI.size()&&(subjectAPI[i].getId()!=-1))
            subjectAPI[i]=subject(i,Data[3],Data[4],stoi(Data[5]));
        if(i>subjectAPI.size())
            subjectAPI.PushBack(subject(i,Data[3],Data[4],stoi(Data[5])));
        subjectAPI.saveToFile(FileSubject);
    }
    else
        throw invalid_argument("Form isn't valid.");
    }
void SelectFunction(const vector<string>& C){
    //svi nazivi tabela imaju razlicit broj karaktera
    switch(C[1].size()){
    case 7:
        SelectTeacher(C[2]);
        break;
    case 8:
        SelectStudent(C[2]);
        break;
    case 10:
        SelectSubject(C[2]);
        break;
    }
    }
void InsertFunction(const vector<string>& C){
    //svi nazivi tabela imaju razlicit broj karaktera
    switch(C[1].size()){
    case 7:
        InsertTeacher(C);
        break;
    case 8:
        InsertStudent(C);
        break;
    case 10:
        InsertSubject(C);
        break;
    }
    }
void UpdateFunction(const vector<string>& C){
    //svi nazivi tabela imaju razlicit broj karaktera
    switch(C[1].size()){
    case 7:
        UpdateTeacher(C[2]); //update funkcije svake od DB ce pozivati odgovarajuce save metode
        break;
    case 8:
        UpdateStudent(C[2]);
        break;
    case 10:
        UpdateSubject(C[2]);
        break;
    }
    }
void DeleteFunction(const vector<string>& C){
    //svi nazivi tabela imaju razlicit broj karaktera
    switch(C[1].size()){
    case 7:
        DeleteTeacher(C[2]);
        break;
    case 8:
        DeleteStudent(C[2]);
        break;
    case 10:
        //DeleteSubject(C[2]);
        break;
    }
    }
int main()
{
    while(1){
    string Input;
    vector<string> Command;
            while(Input != ".") //naredbe se terminiraju sa .
                {
                cin>>Input;
                Command.push_back(Input);
                }
        for(int i=0;i<Command.size();i++)
            cout<<Command[i];
    if((Command[0]=="INSERT") && (Command.size()==3)){
            InsertFunction(Command);
    }else if(Command[0]=="SELECT" && (Command.size()==4)){
            SelectFunction(Command);
    }else if(Command[0]=="UPDATE"){
            UpdateFunction(Command);
    }else if(Command[0]=="DELETE"){
            DeleteFunction(Command);
    }
    else if(Command[0]=="EXIT")
        break;
    else{
        cout<<"Command not valid."<<endl;
    }
    cin.ignore(1000,'\n');
    cin.clear();
    cin.sync();
}

}

