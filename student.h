#ifndef STUDENT_H
#define STUDENT_H
#include<iostream>
#include<string>
#include "person.h"
#include<fstream>
class Student : public Person{
  private:
    int _id;
    int _departmentId;
  public:
    //Member functions
    Student() : _id(-1),_departmentId(-1){}
    ~Student() = default;
    Student(int id,std::string firstName,std::string lastName,std::string birthDate,std::string eMail,std::string gender,std::string jmbg,int departmentId) : Person(firstName,lastName,birthDate,eMail,gender,jmbg),_id(id),_departmentId(departmentId){}
    Student(const Student &other);
    
    //Getters
    int getId(){ return _id; }
    int getDepartmentId(){ return _departmentId; }
    
    //Setters
    void setId(int id){ _id = id; };
    void setDepartmentId(int departmentId){ _departmentId = departmentId; }

    //Delete function
    void deleteStudent();


    //Operators
    friend std::ostream& operator<<(std::ostream& out,const Student& other);
    Student& operator=(const Student& other);
    
    //Print methods
    std::ostream& print(std::ostream& out) const;
    void saveToFile(std::ofstream &) const;
};
Student::Student( const Student &other ){
  _firstName = other._firstName;
  _lastName = other._lastName;
  _birthDate = other._birthDate;
  _eMail = other._eMail;
  _gender = other._gender;
  _jmbg = other._jmbg;
  _id = other._id;
  _departmentId = other._departmentId;
}

Student& Student::operator=(const Student& other){
  _firstName = other._firstName;
  _lastName = other._lastName;
  _birthDate = other._birthDate;
  _eMail = other._eMail;
  _gender = other._gender;
  _jmbg = other._jmbg;
  _id = other._id;
  _departmentId = other._departmentId;

  return *this;
}

void Student::deleteStudent(){
  _id = -1;
  _firstName = " ";
  _lastName = " ";
  _birthDate = " ";
  _eMail = " ";
  _jmbg = " ";
  _departmentId = -1;
}

std::ostream& Student::print(std::ostream& out) const{
  out<<_id<<" "<<_firstName<<" "<<_lastName<<" "<<_birthDate<<" "<<_eMail<<" "<<_gender<<" "<<_jmbg<<" "<<_departmentId<<std::endl;
  return out;
}

void Student::saveToFile(std::ofstream & output) const{
output<<_id<<", "<<_firstName<<", "<<_lastName<<", "<<_birthDate<<", "<<_eMail<<", "<<_gender<<", "<<_jmbg<<", "<<_departmentId<<std::endl;
}
std::ostream& operator<<(std::ostream& out,const Student &other){
  return other.print(out);
}



#endif
