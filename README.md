# README #

Projekat sluzi za kreiranje baze podataka na univerzitetu, koja povezuje studente, profesore, predmete, odsjeke i ocjene. 
Osnovne naredbe za rad programa su:
UPDATE-koji vrsi imjenu postojecih podataka,
SELECT-dohvatanje postojecih podataka,
INSERT-kreiranje novih podataka,
DELETE-brisanje postojecih podataka.

#INCLUDED#

Projekat ukljucuje:
-sve izvorne file-ove (.cpp,.h)
-ULM dijagram
-README file

#INSTALATION#

Program ne zahtijeva dodatnu instalaciju.

#USING#

Sve file-ovi sa podacima se nalaze u folderu.
Podaci niti folderi nisu zasticeni lozinkom.

#IMPLEMENTATION#

Baza podataka bi trebala biti uredjena kao sto je prikazano na prilozenom ULM dijagramu.

#CONTACT#

Projekat su izradjivale:

Meliha Musemić,
Šejla Ćosić,
Emina Turalić,
Aida Lepić,
Ajna Ibrahimkadić.

Ukoliko budete imali nekih problema prilikom pokretanja programa  ili sugestija kontaktirajte nas putem sl. email:
meliha.musemic@fet.ba
sejla.cosic@fet.ba
emina.turalic@fet.ba
aida.lepic@fet.ba
ajna.ibrahimkadic@fet.ba



