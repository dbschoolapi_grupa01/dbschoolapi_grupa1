#ifndef DEPARTMENTSUBJECTS_H
#define DEPARTMENTSUBJECTS_H
#include<string>
#include<iostream>
#include "validate.h"

class DepartmentSubjects { 
protected:
int _departmentId;
int _subjectId;
std::string _studyYear;
std::string _studySemester;
public:
DepartmentSubjects()=default;
~DepartmentSubjects()=default;
DepartmentSubjects(int departmentID, int subjectID, std::string studyYear,std::string studySemester):_departmentId(departmentID), _subjectId(subjectID){ validateStudyYear(studyYear); _studyYear = studyYear; validateStudySemester(studySemester); _studySemester = studySemester; };
DepartmentSubjects(const DepartmentSubjects & other);
std::ostream& print(std::ostream & out)const;

DepartmentSubjects& operator=(const DepartmentSubjects& other);



int& getDepartmentId(){ return _departmentId;}
int& getSubjectId() {return _subjectId;}
std::string& getStudyYear(){return _studyYear;}
std::string& getStudySemester(){return _studySemester;}



void setDepartmendId(int departmentId){ _departmentId=departmentId;}
void setSubjectId(int subjectId) {_subjectId=subjectId;}
void setStudyYear(std::string studyYear){ validateStudyYear(studyYear); _studyYear = studyYear;}
void setStudySemester(std::string studySemester){_studySemester=studySemester; validateStudySemester(studySemester); _studySemester = studySemester; }

};
std::ostream &operator<<(std::ostream & out, const DepartmentSubjects & other);
DepartmentSubjects::DepartmentSubjects(const DepartmentSubjects & other)
{_departmentId=other._departmentId;
 _subjectId=other._subjectId;
 _studyYear=other._studyYear;
 _studySemester=other._studySemester;
}

DepartmentSubjects& DepartmentSubjects::operator=(const DepartmentSubjects & other)
{_departmentId=other._departmentId;
 _subjectId=other._subjectId;
 _studyYear=other._studyYear;
 _studySemester=other._studySemester;
return *this;
}

std::ostream& DepartmentSubjects::print (std::ostream & out) const
{out<<_departmentId<<" "<<_subjectId<<" "<<_studyYear<<" "<<_studySemester<<std::endl;
return out;
}

std::ostream& operator<<(std::ostream & out, const DepartmentSubjects & other)
{
return other.print(out);
}

#endif
