#ifndef DEPARTMENTS_H
#define DEPARTMENTS_H

#include<string>
#include<iostream>
#include<fstream>

/* Declaration */
class departments;
std::ostream & operator<<(std::ostream & , const departments &);

/* Definition */
class departments
{
  private:
        int ID_=-1; //dodato
        std::string Name_;

    public:

    //Member functions
        departments()=default;
        departments(const std::string&);
        ~departments()=default;

    //Getters
         const std::string & getName() const { return Name_; }

       int getID() { return ID_; } //dodato
    //Setters
         void setName(const std::string & Name){ Name_ = Name;}

       void setID(int ID){ ID_=ID; } //dodato
    //Operator
         friend std::ostream & operator<<(std::ostream & , const departments &);
          departments & operator=(const departments &);

    //Print methods
         void saveToFile(std::ofstream &) const;

};
#endif // DEPARTMENTS_H
