#ifndef DEPARTMENTDB_H
#define DEPARTMENTDB_H
#include"departments.h"
#include"ArrayGeneric.h"
#include<string>

class departmentDB:public ArrayGeneric <departments>
{
   public:
       departments& getByID(const int& n){return getPointer()[n];}
       int getID(std::string name); 
       departmentDB& save(int,std::string);
       departmentDB& push(departments&); 

        void saveToFile(const std::string & )  ;
       };

#endif // DEPARTMENTDB_H
