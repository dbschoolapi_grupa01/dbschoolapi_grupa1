void studentDB::saveToFile(const std::string & fileName)
{
  std::ofstream outputfile;
  outputfile.open(fileName);
  if(outputfile.is_open()){
        outputfile<<"Id | firstName | lastName | birthdate | email | gender | jmbg | departmentId"<< std::endl;
        for(int i=1;i<=ArraySize();++i){
                if(getPointer()[i].getId()!=-1){
            getPointer()[i].saveToFile(outputfile);
        }}
          outputfile.close();

  }
  else
  {
    throw std::runtime_error("File didn't open correctly. Closing program !!!\n");
  }
}
