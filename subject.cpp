#include "subject.h"

std::ostream& operator<<(std::ostream& out,const subject& s){
    return out<<s.getSubjectName()<<' '<<s.getSubjectECTS()<<' '<<s.getSubjectAbbreviation()<<std::endl;
}

void subject::saveToFile(std::ofstream & out){
    out<<this->Name<<', '<<this->ECTS<<', '<<this->Abbreviation<<std::endl;
    }
void subject::operator=(const subject& s){
    Name=s.Name;
    ECTS=s.ECTS;
    Abbreviation=s.Abbreviation;
    }

