#ifndef VALIDATE_H
#define VALIDATE_H
#include<string>
#include<exception>

void validateBirthDate(const std::string &date){
  if( date.size() != 10 )
    throw std::string("Invalid argument birth date.The date format has to be yyyy-mm-dd.");
  if( !(date[0] >= '0') && !(date[0] <= '9') )
    throw std::string("Invalid argument birth date.The date format has to be yyyy-mm-dd.");

  if( !(date[1] >= '0') && !(date[1] <= '9') )
    throw std::string("Invalid argument birth date.The date format has to be yyyy-mm-dd.");

  if( !(date[2] >= '0') && !(date[2] <= '9') )
    throw std::string("Invalid argument birth date.The date format has to be yyyy-mm-dd.");

  if( !(date[3] >= '0') && !(date[3] <= '9') )
    throw std::string("Invalid argument birth date.The date format has to be yyyy-mm-dd.");

  if( date[4] != '-' )
    throw std::string("Invalid argument birth date.The date format has to be yyyy-mm-dd.");

  if( !(date[5] >= '0') && !(date[5] <= '9') )
    throw std::string("Invalid argument birth date.The date format has to be yyyy-mm-dd.");

  if( !(date[6] >= '0') && !(date[6] <= '9') )
    throw std::string("Invalid argument birth date.The date format has to be yyyy-mm-dd.");

  if( date[7] != '-' )
    throw std::string("Invalid argument birth date.The date format has to be yyyy-mm-dd.");

  if( !(date[8] >= '0') && !(date[8] <= '9') )
    throw std::string("Invalid argument birth date.The date format has to be yyyy-mm-dd.");

  if( !(date[9] >= '0') && !(date[9] <= '9') )
    throw std::string("Invalid argument birth date.The date format has to be yyyy-mm-dd.");
}

void validateEmail(const std::string &mail){
  std::string temp;
  int count = mail.size() - 7;
  for(int i = mail.size()-1;i>count-1; --i){
     temp.push_back(mail.at(i));
  }
  if(temp != "ab.tef@")
    throw std::string("Invalid argument eMail.The mail address must contain @fet.ba domain.");
}

void validateGender(const std::string &gender){
  if( gender != "F" && gender != "M" )
    throw std::string("Invalid argument gender.Input 'F' for female or 'M' for male.");
}

void validateJMBG(const std::string &jmbg){
  if( jmbg.size() != 13)
    throw std::string("Invalid argument JMBG.The JMBG must contain 13 numbers.");
  for(auto elem : jmbg){
     if( !(elem >= '0') && !(elem <= '9') )
      throw std::string("Invalid argument JMBG.The JMBG must contain 13 numbers.");
  }  
}

void validateTitle(const std::string &title){
  if(title != "dr.sc (van. prof.)" && title != "dr.sc (red. prof.)" && title != "dr.sc (docent)")
    throw std::string("Invalid argument title.The title has to be in format dr.sc (van. prof.) or dr.sc (red. prof.) or dr.sc (docent).");
}

void validateDate(const std::string &date){
  if( date.size() != 10 )
    throw std::string("Invalid argument date.The date format has to be yyyy-mm-dd.");

  if( !(date[0] >= '0') && !(date[0] <= '9') )
    throw std::string("Invalid argument date.The date format has to be yyyy-mm-dd.");

  if( !(date[1] >= '0') && !(date[1] <= '9') )
    throw std::string("Invalid argument date.The date format has to be yyyy-mm-dd.");

  if( !(date[2] >= '0') && !(date[2] <= '9') )
    throw std::string("Invalid argument date.The date format has to be yyyy-mm-dd.");

  if( !(date[3] >= '0') && !(date[3] <= '9') )
    throw std::string("Invalid argument date.The date format has to be yyyy-mm-dd.");

  if( date[4] != '-' )
    throw std::string("Invalid argument date.The date format has to be yyyy-mm-dd.");

  if( !(date[5] >= '0') && !(date[5] <= '9') )
    throw std::string("Invalid argument date.The date format has to be yyyy-mm-dd.");

  if( !(date[6] >= '0') && !(date[6] <= '9') )
    throw std::string("Invalid argument date.The date format has to be yyyy-mm-dd.");

  if( date[7] != '-' )
    throw std::string("Invalid argument date.The date format has to be yyyy-mm-dd.");

  if( !(date[8] >= '0') && !(date[8] <= '9') )
    throw std::string("Invalid argument date.The date format has to be yyyy-mm-dd.");

  if( !(date[9] >= '0') && !(date[9] <= '9') )
    throw std::string("Invalid argument date.The date format has to be yyyy-mm-dd.");
}

void validateStudyYear(const std::string &year){
  if( year != "I" && year != "II" && year != "III" && year != "IV" )
    throw std::string("Invalid argument study year.The study year format has to be in format 'I' or 'II' or 'III' or 'IV'.");
}

void validateStudySemester(const std::string &semester){
  if( semester != "I" && semester != "II" && semester != "III" && semester != "IV" && semester != "V" && semester != "VI" && semester != "VII" && semester != "VIII")
    throw std::string("Invalid argument study semester.The study semester format has to be in format 'I' or 'II' or 'III' or 'IV' or 'V' or 'VI' or 'VII' or 'VIII'.");
}
#endif
