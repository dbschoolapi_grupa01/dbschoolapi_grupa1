#ifndef TEACHER_H
#define TEACHER_H
#include<iostream>
#include<string>
#include "person.h"
#include "validate.h"
#include<fstream>
class Teacher : public Person{
  private:
    std::string _title;
    int _departmentId;
    int _id;
  public:
    //Member functions
    Teacher() : _id(-1),_departmentId(-1){}
    ~Teacher() = default;
    Teacher(int id,std::string firstName,std::string lastName,std::string birthDate,std::string eMail,std::string gender,std::string jmbg,std::string title,int departmentId) : Person(firstName,lastName,birthDate,eMail,gender,jmbg),_id(id),_departmentId(departmentId){ validateTitle(title); _title = title;}
    Teacher(const Teacher &other);
    
    //Getters
    std::string getTitle(){ return _title; }
    int getId(){ return _id; }
    int getDepartmentId(){ return _departmentId; }
    
    //Setters
    void setTitle(std::string title){ validateTitle(title); _title = title; }
    void setId(int id){ _id = id; };
    void setDepartmentId(int departmentId){ _departmentId = departmentId; }
    
    //Print methods
    std::ostream& print(std::ostream& out) const;
    
    //Operators
    friend std::ostream& operator<<(std::ostream &out,const Teacher &other);
   Teacher& operator=(const Teacher& other);
   
   //Delete function 
    void deleteTeacher();

    void saveToFile(std::ofstream &) const;


};

Teacher::Teacher(const Teacher &other){
  _firstName = other._firstName;
  _lastName = other._lastName;
  _birthDate = other._birthDate;
  _eMail = other._eMail;
  _gender = other._gender;
  _jmbg = other._jmbg;
  _title = other._title;
  _id = other._id;
  _departmentId = other._departmentId;
}

Teacher& Teacher::operator=(const Teacher& other){
  _firstName = other._firstName;
  _lastName = other._lastName;
  _birthDate = other._birthDate;
  _eMail = other._eMail;
  _gender = other._gender;
  _jmbg = other._jmbg;
  _title = other._title;
  _id = other._id;
  _departmentId = other._departmentId;

  return *this;
}

std::ostream& Teacher::print(std::ostream& out) const{
  out<<_id<<" "<<_firstName<<" "<<_lastName<<" "<<_birthDate<<" "<<_eMail<<" "<<_gender<<" "<<_jmbg<<" "<<_title<<" "<<_departmentId<<std::endl;
  return out;
}

std::ostream& operator<<(std::ostream& out,const Teacher& other){
  return other.print(out);
}


void Teacher::deleteTeacher(){
  _id = -1;
  _firstName = " ";
  _lastName = " ";
  _birthDate = " ";
  _eMail = " ";
  _gender = " ";
  _jmbg = " ";
  _title = " ";
  _departmentId = -1;
}

void Teacher::saveToFile(std::ofstream & output) const{
output<<_id<<", "<<_firstName<<", "<<_lastName<<", "<<_birthDate<<", "<<_eMail<<", "<<_gender<<", "<<_jmbg<<", "<<_title<<", "<<_departmentId<<std::endl;

}
#endif
