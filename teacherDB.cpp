#include "teacherDB.h"

/*teacherDB& teacherDB::save(const int& i,const std::string& n,const int& e){
    if(){
        getPointer()[i-1].setTitle(n);

        if(e==subjectB::name::Sizee())
        getPointer()[i-1].setDepartmentId(e);

        return *this;
    }
    }
*/
void teacherDB::saveToFile(const std::string & fileName)
{
  std::ofstream outputfile;
  outputfile.open(fileName);
  if(outputfile.is_open()){
        outputfile<<"Id | firstName | lastName | birthdate | email | gender | jmbg | title | departmentId"<< std::endl;
        for(int i=1;i<=ArraySize();++i){
                if(getPointer()[i].getId()!=-1){
            getPointer()[i].saveToFile(outputfile);
        }}
          outputfile.close();

  }
  else
  {
    throw std::runtime_error("File didn't open correctly. Closing program !!!\n");
  }
}

Teacher::Teacher(const Teacher &other){
  _firstName = other._firstName;
  _lastName = other._lastName;
  _birthDate = other._birthDate;
  _eMail = other._eMail;
  _gender = other._gender;
  _jmbg = other._jmbg;
  _title = other._title;
  _id = other._id;
  _departmentId = other._departmentId;
}

Teacher& Teacher::operator=(const Teacher& other){
  _firstName = other._firstName;
  _lastName = other._lastName;
  _birthDate = other._birthDate;
  _eMail = other._eMail;
  _gender = other._gender;
  _jmbg = other._jmbg;
  _title = other._title;
  _id = other._id;
  _departmentId = other._departmentId;

  return *this;
}

std::ostream& Teacher::print(std::ostream& out) const{
  out<<_id<<" "<<_firstName<<" "<<_lastName<<" "<<_birthDate<<" "<<_eMail<<" "<<_gender<<" "<<_jmbg<<" "<<_title<<" "<<_departmentId<<std::endl;
  return out;
}

std::ostream& operator<<(std::ostream& out,const Teacher& other){
  return other.print(out);
}

void Teacher::saveToFile(std::ostream & output) const{
output<<_id<<', '<<_firstName<<', '<<_lastName<<', '<<_birthDate<<', '<<_eMail<<', '<<_gender<<', '<<_jmbg<<', '<<_title<<', '<<_departmentId<<std::endl;
}


