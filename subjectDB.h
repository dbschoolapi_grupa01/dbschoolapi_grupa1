#ifndef SUBJECTDB_H
#define SUBJECTDB_H
#include"subject.h"
#include"ArrayGeneric.h"

class subjectDB : public ArrayGeneric<subject>
{
    public:
        subject& getByID(const int& n){
            return getPointer()[n];}
        subjectDB& save(const int&,const std::string&,const int&,const std::string&);
        void saveToFile(const std::string & )  ;
    protected:

    private:
};

#endif // SUBJECTDB_H
