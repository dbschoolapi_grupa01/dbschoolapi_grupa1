#ifndef ARRAYGENERIC_H
#define ARRAYGENERIC_H
#include<initializer_list>
#include<iostream>
#include<string>
#include<stdexcept>
#include<algorithm>

template<typename Type>
class ArrayGeneric
{
private:
    int Bottom,Top;
    int Size=0;
    int Capacity=0;
    Type * Elements;
    void Allocate(int,int);
    void Copy(const ArrayGeneric&);
public:
    Type* getPointer(){return Elements;}
    ArrayGeneric():Capacity(8),Size(0),Top(0),Bottom(0){Elements=new Type[Capacity];};
    ArrayGeneric(int BottomTemp,int TopTemp);
    ArrayGeneric(int SizeTemp);
    ArrayGeneric(const ArrayGeneric&);
    ArrayGeneric(ArrayGeneric&&);
    ArrayGeneric(std::initializer_list<Type>, int=0);
    ~ArrayGeneric(){delete [] Elements;}
    ArrayGeneric & operator=(const ArrayGeneric&);
    ArrayGeneric & operator=(ArrayGeneric&&);
    Type & operator[](int Index);
    const Type & operator[](int Index) const;
    void ReAllocate(int BottomTemp,int TopTemp);
    int FindBottom() const {return Bottom;};
    int FindTop() const {return Top;};
    int ArraySize() const {return Size;};
    int size() const {return Size;};
    void print(std::ostream&) const;
    void PushBack(const Type & t);
    void increment() { ++Size; }; // dodato - povecava velicinu niza
    class iterator;
    iterator begin(){return iterator(Elements);}
    iterator end(){return iterator(Elements+Size);}
};

#endif // ARRAYGENERIC_H


template<typename Type>
void ArrayGeneric<Type>::PushBack(const Type& t){
if(Size==Capacity){
    ReAllocate(Bottom,Top+8);
    Elements[++Size]=t;
    Top=Size;
}
else{
    Elements[++Size]=t;
    Top=Size;
}
}

template<typename Type>
void ArrayGeneric<Type>::Allocate(int d, int g)
{
  if(g < d)
    throw std::invalid_argument("Donja granica ne smije biti veca od gornje.");

  Size = 0;
  Capacity=g-d+1;
  Elements = new Type[Capacity];
  Bottom = d;
  Top = g;
}

template<typename Type>
void ArrayGeneric<Type>::Copy(const ArrayGeneric<Type>& b)
{
  for(int i=1; i<=Size; i++)
    Elements[i] = b.Elements[i];
}

template<typename Type>
void ArrayGeneric<Type>::print(std::ostream& o) const
{
  for(int i=1; i<=Size; i++)
    o << Elements[i];
}

template <typename Type>
ArrayGeneric<Type>::ArrayGeneric(int d, int g)
{
  Allocate(d, g);
}

template <typename Type>
ArrayGeneric<Type>::ArrayGeneric(int n)
{
  Allocate(1, n);
}

template <typename Type>
ArrayGeneric<Type>::ArrayGeneric(const ArrayGeneric<Type>& b)
{
  Allocate(b.Bottom, b.Top);
  Copy(b);
}

template <typename Type>
ArrayGeneric<Type>::ArrayGeneric(ArrayGeneric<Type>&& b)
{
  Size=b.Size;
  Bottom=b.Bottom;
  Top=b.Top;
  Elements=b.Elements;
  b.Elements=nullptr;
}

template <typename Type>
ArrayGeneric<Type>::ArrayGeneric(std::initializer_list<Type> il, int poc)
{
   Size=il.size();
   if(Capacity<Size){
   Bottom=poc;
   Top=poc + Size - 1;
   Elements=new Type[Size];
   Capacity=Size;
   std::copy(il.begin(),il.end(),begin());
}else{
Bottom=poc;
Top=poc+Size-1;
std::copy(il.begin(),il.end(),begin());
}
}



template <typename Type>
ArrayGeneric<Type>& ArrayGeneric<Type>::operator=(ArrayGeneric<Type>&& b){
 delete[] Elements;
 Size=b.Size;
 Bottom=b.Bottom;
 Top=b.Top;
 Elements=b.Elements;
 b.Elements=nullptr;
 return *this;
}

template <typename Type>
ArrayGeneric<Type>& ArrayGeneric<Type>::operator=(const ArrayGeneric<Type>& b)
{
    delete [] Elements;
    Allocate(b.Bottom,b.Top);
    Copy(b);

    return *this;
}

template <typename Type>
Type & ArrayGeneric<Type>::operator[](int index)
{
    if(index<Bottom || Top<index){
    std::string poruka="Index izvan granica";
    throw std::invalid_argument(poruka);
    }
    return Elements[index-Bottom];
}

template <typename Type>
const Type & ArrayGeneric<Type>::operator[](int index) const
{
    if(index<Bottom || Top<index){
    std::string poruka="Index izvan granica.";

    throw std::invalid_argument(poruka);
    }
    return Elements[index-Bottom];
}

template <typename Type>
void ArrayGeneric<Type>::ReAllocate(int d, int g)
{
    if(g<d)
    throw std::invalid_argument("Donja granica ne smije biti veca od gornje.");

    Type * NewArray=new Type[g-d+1];

    if(d<=Top && g>=Bottom){
        int dCarry, gCarry;
        if(Bottom<d)
            dCarry=d;
        else
            dCarry=Bottom;
        if(Top>g)
            gCarry=g;
        else
            gCarry=Top;
        for(int i=dCarry;i<=gCarry;i++){
            NewArray[i-d]=Elements[i-Bottom];}
    }

    delete [] Elements;
    Elements=NewArray;
    Bottom=d;
    Top=g;
    Capacity=g-d+1;
}

template<typename Type>
class ArrayGeneric<Type>::iterator : public std::iterator<std::random_access_iterator_tag, Type>
{
  private:
    Type * pel;
  public:
     using difference_type=typename std::iterator<std::random_access_iterator_tag, Type>::difference_type;
    // typedef std::bidirectional_iterator_tag iterator_category;
    // typedef Elem value_type;
    // typedef size_t difference_type;
    // typedef Elem* pointer;
    // typedef Elem& reference;

    iterator(Type *p) : pel(p){}
    Type& operator*(){return *pel;}
    iterator& operator++(){++pel; return *this ;}
    iterator operator++(int){
      iterator temp = *this;
      ++pel;
      return temp;
    }
    iterator& operator--(){--pel; return *this ;}
    iterator operator--(int){
      iterator temp = *this;
      --pel;
      return temp;
    }
    bool operator!=(const iterator& b){return pel != b.pel;}
    bool operator==(const iterator& b){return pel == b.pel;}
    bool operator<(const iterator& b){return pel<b.pel;}
    bool operator>(const iterator& b){return pel>b.pel;}
    bool operator<=(const iterator& b){return pel<=b.pel;}
    bool operator>=(const iterator& b){return pel<=b.pel;}

    iterator& operator+=(difference_type &n){pel+=n; return *this;}
    iterator& operator-=(difference_type &n){pel-=n; return *this;}
    iterator operator+(difference_type n){return pel+n;}
    iterator operator-(difference_type n){return pel-n;}
    //iterator operator+(difference_type n,const iterator& b){return pel+n;}
    //iterator operator-(difference_type n,const iterator& b){return pel-n;}
    difference_type operator-(const iterator& b){return pel-b.pel;}
    Type & operator[](difference_type n){return pel[n];}
};

