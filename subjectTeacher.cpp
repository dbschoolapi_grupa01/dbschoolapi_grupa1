#include "subjectTeacher.h"

void subjectTeacherDB::insert(const int& s,const int& n){
    Node* current=first;
    Node* trailCurrent;
    bool found=false;
        if(first==nullptr){
        Node* newNode=new Node;
        (*newNode).setSubjectID(s);
        (*newNode).addTeacher(n);
        (*newNode).setLink(nullptr);
        last=newNode;
        first=newNode;
        Size++;
}
else{
    Node* temp=(*this).find(s);
    if(temp!=nullptr){
    (*temp).addTeacher(n);
    }
    else{
    Node* newNode=new Node;
    (*newNode).setSubjectID(s);
    (*newNode).addTeacher(n);

    while (current != nullptr && !found)
			if ((*current).getSubjectID() >= s)
				found = true;
			else
			{
				trailCurrent = current;
				current = (*current).getLink();
			}
    if (current == first)
		{
		    (*newNode).setLink(first);
			first = newNode;
			Size++;
		}
		else
		{
			(*trailCurrent).setLink(newNode);
			(*newNode).setLink(current);

			if (current == nullptr)
				last = newNode;

			Size++;
		}
	}
}
}


subjectTeacherDB::~subjectTeacherDB(){
destroyList();
}
void subjectTeacherDB::destroyList(){
Node *temp;
    while (first != nullptr)
	{
		temp = first;
		first = (*first).getLink();
		delete temp;
	}

	last = nullptr;
	Size = 0;
}

Node* subjectTeacherDB::find(const int& s){
Node* temp=first;
while(temp!=nullptr){
    if((*temp).getSubjectID()==s)
        break;
    temp=(*temp).getLink();
}
return temp;
}

void subjectTeacherDB::ispis(){
Node* temp=first;
while(temp!=nullptr){
    for(int i=0;i<temp->getTeachers().velicina();++i){
      cout<<temp->getSubjectID()<<", ";
      cout<<temp->getTeachers()[i]<< endl;
    }

    temp=(*temp).getLink();
}
}


void subjectTeacherDB::saveToFile(const std::string & fileName)
{
  std::ofstream outputfile;
  outputfile.open(fileName);
  if(outputfile.is_open()){
        outputfile<<"SubjectId | TeacherId "<< std::endl;
    Node* temp=first;
    while(temp!=nullptr){
            temp->saveToFile(outputfile);
            temp=(*temp).getLink();
        }
          outputfile.close();

  }
  else
  {
    throw std::runtime_error("File didn't open correctly. Closing program !!!\n");
  }
}
