#ifndef SUBJECT_H
#define SUBJECT_H
#include<string>
#include<iostream>
#include<fstream>

class subject
{
    public:
        subject(){}
        subject(const int& n,const std::string& N="",const std::string& A="",const int& E=5):Name(N),Abbreviation(A),ECTS(E){
        if(Name!="")
            id=n;
        }
        std::string getSubjectName()const{return Name;}
        std::string getSubjectAbbreviation()const{return Abbreviation;}
        int getSubjectECTS()const{return ECTS;}
        int getId(){return id;}

        void setSubjectName(const std::string& N){Name=N;}
        void setSubjectAbbreviation(const std::string& A){Abbreviation=A;}
        void setSubjectECTS(const int& E){ECTS=E;}
        void setId(int n){id=n;}
        //subject(subject&&);
        //subject(const subject&);
        void saveToFile(std::ofstream &);
        void operator=(const subject&);

    private:
        std::string Name, Abbreviation;
        int ECTS;
        int id=-1;
};

std::ostream& operator<<(std::ostream& out,const subject& s);
#endif // SUBJECT_H
